package id.co.nexsoft.summative4.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.summative4.model.User;
import id.co.nexsoft.summative4.repository.UserRepository;
import id.co.nexsoft.summative4.service.DefaultService;

@Service
public class UserServiceImpl implements DefaultService<User> {
    @Autowired
    private UserRepository repository;

    @Override
    public List<User> getAllData() {
        return repository.findAll();
    }

    @Override
    public Optional<User> getDataById(Long id) {
        return repository.findById(id);
    }

    @Override
    public boolean deleteDataById(Long id) {
        Optional<User> dataPut = repository.findById(id);
        if (dataPut.isPresent()) {
            repository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public User putData(User data, Long id) {
        Optional<User> dataPut = repository.findById(id);
        if (dataPut.isPresent()) {
            data.setId(id);
            return repository.save(data);
        }
        return null;
    }

    @Override
    public User postData(User data) {
        return repository.save(data);
    }

    @Override
    public User getDataByName(String name) {
        return repository.findByName(name);
    }
}
