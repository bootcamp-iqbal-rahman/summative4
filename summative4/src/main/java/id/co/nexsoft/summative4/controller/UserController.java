package id.co.nexsoft.summative4.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.summative4.model.User;
import id.co.nexsoft.summative4.service.DefaultService;
import id.co.nexsoft.summative4.utils.StatusCode;
import jakarta.validation.Valid;

@RestController
@RequestMapping("api/user")
@Validated
public class UserController extends StatusCode {
    @Autowired
    private DefaultService<User> service;

    @GetMapping
    public ResponseEntity<?> getAllData() {
        List<User> data = service.getAllData();
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getDataById(@PathVariable Long id) {
        Optional<User> data = service.getDataById(id);
        return (!data.isPresent()) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(data, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> postData(@Valid @RequestBody User data) {
        User dataPost = service.postData(data);
        return (dataPost == null) ? 
            new ResponseEntity<>(get422(), HttpStatus.UNPROCESSABLE_ENTITY) : 
            new ResponseEntity<>(get201(), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> putData(@Valid @RequestBody User data, @PathVariable Long id) {
        User dataPut = service.putData(data, id);
        return (dataPut == null) ?
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(dataPut, HttpStatus.OK);
    }   

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteData(@PathVariable Long id) {
        boolean dataDelete = service.deleteDataById(id);
        return (!dataDelete) ? 
            new ResponseEntity<>(get404(), HttpStatus.NOT_FOUND) : 
            new ResponseEntity<>(get200(), HttpStatus.OK);
    }
}
