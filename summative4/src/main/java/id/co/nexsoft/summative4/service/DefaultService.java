package id.co.nexsoft.summative4.service;

import java.util.List;
import java.util.Optional;

public interface DefaultService<T> {
    List<T> getAllData();
    Optional<T> getDataById(Long id);
    boolean deleteDataById(Long id);
    T putData(T data, Long id);
    T postData(T data);
    T getDataByName(String name);
}
