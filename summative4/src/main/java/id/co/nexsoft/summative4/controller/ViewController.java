package id.co.nexsoft.summative4.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequestMapping("/")
public class ViewController {
    
    @GetMapping
    public String index() {
        return "index";
    }
    
}
