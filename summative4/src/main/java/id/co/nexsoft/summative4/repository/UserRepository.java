package id.co.nexsoft.summative4.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.summative4.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByName(String name);
}
