package id.co.nexsoft.summative4.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.nexsoft.summative4.model.Department;
import id.co.nexsoft.summative4.repository.DepartmentRepository;
import id.co.nexsoft.summative4.service.DefaultService;

@Service
public class DepartmentServiceImpl implements DefaultService<Department> {
    @Autowired
    private DepartmentRepository repository;

    @Override
    public List<Department> getAllData() {
        return repository.findAll();
    }

    @Override
    public Optional<Department> getDataById(Long id) {
        return repository.findById(id);
    }

    @Override
    public boolean deleteDataById(Long id) {
        Optional<Department> dataPut = repository.findById(id);
        if (dataPut.isPresent()) {
            repository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public Department putData(Department data, Long id) {
        Optional<Department> dataPut = repository.findById(id);
        if (dataPut.isPresent()) {
            data.setId(id);
            return repository.save(data);
        }
        return null;
    }

    @Override
    public Department postData(Department data) {
        return repository.save(data);
    }

    @Override
    public Department getDataByName(String name) {
        return repository.findByName(name);
    }
}
