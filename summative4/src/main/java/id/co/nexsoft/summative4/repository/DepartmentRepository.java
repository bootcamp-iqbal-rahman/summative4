package id.co.nexsoft.summative4.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.nexsoft.summative4.model.Department;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
    Department findByName(String name);
}
