const URL_DEPARTMENT = "http://localhost:8080/api/department"
const listTable = [
    "name", "last_name", "email", "birth_date", "department"
]
const typeTable = [
    "text", "text", "text", "date", "text" 
]
const labelTable = [
    "Name", "Last Name", "Email", "Birth Date", "Department"
]


let dataDepartmentLoc = {}

let updatedKey

async function changeId(value) {
    const lowercaseTarget = value.toLowerCase();
    console.log(dataDepartmentLoc);

    let matchFound = false;
    let latestId = 0;
    for (const key in dataDepartmentLoc) {
        if (dataDepartmentLoc.hasOwnProperty(key) && dataDepartmentLoc[key].toLowerCase() === lowercaseTarget) {
            matchFound = true;
            updatedKey = key;
            return;
        }
        latestId = key;
    }

    await postDepartment(value);

    await getData(URL_DEPARTMENT).then(dataDepart => {
        dataDepart.forEach(element => {
            dataDepartmentLoc[element.id] = element.name
            if (element.id > latestId) {
                updatedKey = element.id;
            }
        });
    })
}

async function postDepartment(name) {
    const data = {
        name: name
    };

    try {
        const response = await fetch(URL_DEPARTMENT, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (!response.ok) {
            throw new Error('Network response was not ok.');
        }

        const responseData = await response.json();
        console.log(responseData);
    } catch (error) {
        console.error('There was a problem with the fetch operation:', error);
    }
}

async function putData(index, id) {
    const nameField = document.getElementById(`name${index}`).value;
    const lastNameField = document.getElementById(`last_name${index}`).value;
    const emailField = document.getElementById(`email${index}`).value;
    const birthDateField = document.getElementById(`birth_date${index}`).value;
    const departmentField = document.getElementById(`department${index}`).value;
    await changeId(departmentField);
    const departmentId = updatedKey;

    const url = 'http://localhost:8080/api/user/' + id;
    const data = {
        name: nameField,
        lastName: lastNameField,
        email: emailField,
        birthDate: birthDateField,
        departmentId: departmentId
    };

    try {
        const response = await fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (!response.ok) {
            throw new Error('Network response was not ok.');
        }

        const responseData = await response.json();
        console.log(responseData);
    } catch (error) {
        console.error('There was a problem with the fetch operation:', error);
    }
}

async function postData(index) {
    const nameField = document.getElementById(`name${index}`).value;
    const lastNameField = document.getElementById(`last_name${index}`).value;
    const emailField = document.getElementById(`email${index}`).value;
    const birthDateField = document.getElementById(`birth_date${index}`).value;
    const departmentField = document.getElementById(`department${index}`).value;
    await changeId(departmentField);
    const departmentId = updatedKey;

    const url = 'http://localhost:8080/api/user';
    const data = {
        name: nameField,
        lastName: lastNameField,
        email: emailField,
        birthDate: birthDateField,
        departmentId: departmentId
    };

    try {
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (!response.ok) {
            throw new Error('Network response was not ok.');
        }

        const responseData = await response.json();
        console.log(responseData);
    } catch (error) {
        console.error('There was a problem with the fetch operation:', error);
    }
}

async function deleteData(index) {
    const url = 'http://localhost:8080/api/user/' +index;

    try {
        const response = await fetch(url, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (!response.ok) {
            throw new Error('Network response was not ok.');
        }

        const responseData = await response.json();
        console.log(responseData);
    } catch (error) {
        console.error('There was a problem with the fetch operation:', error);
    }
}

function createTableRow(id, data) {
    const row = document.createElement('tr');

    for (let i = 0; i < 5; i++) {
        const td = document.createElement('td');
        td.className = 'border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4';

        const div = document.createElement('div');
        const label = document.createElement('label');
        label.htmlFor = listTable[i] + id;
        label.className = 'block mb-2 text-sm font-medium text-gray-900 dark:text-white sr-only';
        label.textContent = labelTable[i];

        const input = document.createElement('input');
        input.type = typeTable[i];
        input.id = listTable[i] + id;
        input.className = 'bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500';
        input.placeholder = labelTable[i];
        input.value = data[i];
        input.required = true;

        div.appendChild(label);
        div.appendChild(input);
        td.appendChild(div);
        row.appendChild(td);
    }

    const buttonCell = document.createElement('td');
    buttonCell.className = 'border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4';

    const updateButton = document.createElement('button');
    updateButton.type = 'button';
    updateButton.className = 'text-yellow-400 hover:text-white border border-yellow-400 hover:bg-yellow-500 focus:ring-4 focus:outline-none focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2 dark:border-yellow-300 dark:text-yellow-300 dark:hover:text-white dark:hover:bg-yellow-400 dark:focus:ring-yellow-900';
    updateButton.textContent = 'Update';
    updateButton.onclick = function(){
        if (data[5]) {
            putData(id, data[5]);            
        } else {
            postData(id);
        }
    };

    const deleteButton = document.createElement('button');
    deleteButton.type = 'button';
    deleteButton.className = 'text-red-700 hover:text-white border border-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2 dark:border-red-500 dark:text-red-500 dark:hover:text-white dark:hover:bg-red-600 dark:focus:ring-red-900';
    deleteButton.textContent = 'Delete';
    deleteButton.onclick = function(){
        if (data[5]) {
            deleteData(data[5]);
        }
    };

    buttonCell.appendChild(updateButton);
    buttonCell.appendChild(deleteButton);
    row.appendChild(buttonCell);

    return row;
}

const container = document.getElementById('table-body');

function createTableData(data, dataDepart) {
    dataDepartmentLoc = dataDepart
    const lengthData = data.length;
    for (let i = 0; i < lengthData; i++) {
        const resultData = [
            data[i].name,
            data[i].lastName,
            data[i].email,
            data[i].birthDate,
            dataDepart[data[i].departmentId],
            data[i].id
        ]
        const table = createTableRow(i+1, resultData);
        container.appendChild(table);
    }
    for (let i = 0; i < 10; i++) {
        const resultData = [
            null,null,null,null,null,null
        ]
        const table = createTableRow(lengthData+1+i,resultData);
        container.appendChild(table);
    }
}

const getData = (urlApi) => {
    return new Promise((resolve, reject) => {
        fetch(urlApi)
            .then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status} - ${response.statusText}`)
                }
                return response.json()
            })
            .then(data => resolve(data))
            .catch(error => reject(error))
    })
}

export default createTableData;