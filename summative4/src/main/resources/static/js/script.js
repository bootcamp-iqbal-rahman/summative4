import createTableData from './createTable.js';

const getData = (urlApi) => {
    return new Promise((resolve, reject) => {
        fetch(urlApi)
            .then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status} - ${response.statusText}`)
                }
                return response.json()
            })
            .then(data => resolve(data))
            .catch(error => reject(error))
    })
}

const URL_USER = "http://localhost:8080/api/user"
const URL_DEPARTMENT = "http://localhost:8080/api/department"

window.loadApi = loadApi()

let dataMap = {}

function loadApi() {
    getData(URL_USER).then(data => {
        getData(URL_DEPARTMENT).then(dataDepart => {
            dataDepart.forEach(element => {
                dataMap[element.id] = element.name
            });
            createTableData(data, dataMap)
        })
    })
}