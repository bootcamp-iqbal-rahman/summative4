package id.co.nexsoft.summative4;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import id.co.nexsoft.summative4.model.User;

@SpringBootTest
@AutoConfigureMockMvc
public class UserApplicationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void getAllData() throws Exception {
        this.mockMvc.perform(get("/api/user"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getDataById() throws Exception {
        this.mockMvc.perform(get("/api/user/{id}", 1))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void getDataByNullId() throws Exception {
        this.mockMvc.perform(get("/api/user/{id}", 100))
            .andDo(print())
            .andExpect(status().isNotFound());
    }

    @Test
    void postData() throws Exception {
        User mokcUser = new User(
            "test", "test_last", "test@gmail.com", LocalDate.now(), 1
        );

        mockMvc.perform(post("/api/user")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mokcUser))
        ).andExpect(status().isCreated());
    }

    @Test
    void putData() throws Exception {
        User mokcUser = new User("test", "test_name", "test@gmail.com", LocalDate.now(), 1);
        
        mockMvc.perform(put("/api/user/{id}", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mokcUser))
        ).andExpect(status().isOk());
    }

    @Test
    void putDataNullId() throws Exception {
        User mokcUser = new User("test", "test_name", "test@gmail.com", LocalDate.now(), 1);
        
        mockMvc.perform(put("/api/user/1000")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(mokcUser))
        ).andExpect(status().isNotFound());
    }

    @Test
    void deleteData() throws Exception {
        mockMvc.perform(delete("/api/user/{id}", 18)
                ).andExpect(status().isOk());
    }

    @Test
    void deleteNullData() throws Exception {
        mockMvc.perform(delete("/api/user/{id}", 1000)
                ).andExpect(status().isNotFound());
    }

    private String asJsonString(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper.writeValueAsString(object);
    }
}