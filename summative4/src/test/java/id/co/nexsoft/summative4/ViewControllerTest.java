package id.co.nexsoft.summative4;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import id.co.nexsoft.summative4.controller.ViewController;

@WebMvcTest(ViewController.class)
public class ViewControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void indexView() throws Exception {
        mockMvc.perform(get("/"))
                    .andExpect(status().isOk())
                    .andExpect(view().name("index"));
    }
}
