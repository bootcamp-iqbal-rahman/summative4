package id.co.nexsoft.summative4;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Summative4ApplicationTests {

	@Test
	void contextLoads() {
		Summative4Application.main(new String[]{});
	}

}
